package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// var mutex = &sync.Mutex{}

type httpEndpoint struct {
	crisp *crisp
}

type SubmitButtonItem struct {
	Action    Action  `json:"action"`
	Widget    Widget  `json:"widget"`
	Origin    Origin  `json:"origin"`
	Payload   Payload `json:"payload"`
	Timestamp int     `json:"timestamp"`
}

type Action struct {
	Type   string `json:"type"`
	Method string `json:"method"`
}

type Widget struct {
	SectionID string `json:"section_id"`
	ItemID    string `json:"item_id"`
}

type Origin struct {
	WebsiteID string `json:"website_id"`
	SessionID string `json:"session_id"`
}

type Payload struct {
	Value Value `json:"value"`
	Data  Data  `json:"data"`
}

type Value struct {
	DataUserCreationDate string `json:"data_user_creation_date"`
	DataUserID           string `json:"data_user_id"`
}

type Data struct {
	OperatorId        string `json:"operatror_id"`
	OperatorFirstname string `json:"operator_firstname"`
}

func putRequest(url string, data io.Reader) *http.Response {
	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPut, url, data)
	if err != nil {
		log.Fatal(err)
	}
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	return res
}

func (httpEndpointManager *httpEndpoint) exportConversation(w http.ResponseWriter, r *http.Request) {

	var res SubmitButtonItem

	data, _ := ioutil.ReadAll(r.Body)

	err := json.Unmarshal([]byte(data), &res)

	if err != nil {
		fmt.Printf("%v \n", err)
	}

	fmt.Printf("Operator: %v\n", res.Payload.Data.OperatorId)

	httpEndpointManager.crisp.getTranscript(res)
	httpEndpointManager.crisp.generateBucket(res.Origin.SessionID, res.Payload.Data.OperatorId)
}

func (crispManager *crisp) initHTTPEndpoint() error {
	httpEndpointManager := &httpEndpoint{crisp: crispManager}

	http.HandleFunc("/export", httpEndpointManager.exportConversation)
	http.Handle("/", http.FileServer(http.Dir("./public")))

	fmt.Printf("Listening on: %v\n", crispManager.config.HTTPAPIListen)
	return http.ListenAndServe(crispManager.config.HTTPAPIListen, nil)
}
