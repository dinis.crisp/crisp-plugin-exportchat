package main

import (
	"fmt"
	"os"

	crispAPI "github.com/crisp-im/go-crisp-api/crisp"
)

func (crispManager *crisp) generateBucket(sessionID string, operatorID string) {

	file := crispAPI.BucketURLRequestFile{
		Name: string(sessionID + ".txt"),
		Type: "text/plain",
	}

	bucketData := crispAPI.BucketURLRequest{
		Namespace:  "upload",
		From:       "plugin",
		Identifier: "b54e1082-8acd-4558-b0a6-db06ee767b60",
		ID:         sessionID,
		File:       file,
	}

	_, err := crispManager.client.Bucket.GenerateBucketURL(bucketData)

	if err != nil {
		fmt.Printf("Bucket Error: %v\n", err)
	}

}

func (crispManager *crisp) sendFile(url string, websiteID string, sessionID string) {

	content := crispAPI.ConversationFileMessageNewContent{
		Name: string("Chat Transcript: " + sessionID),
		URL:  url,
		Type: "text/plain",
	}
	user := crispAPI.ConversationAllMessageNewUser{Type: "website", Nickname: "Transcripts", Avatar: "https://storage.crisp.chat/users/avatar/website/754190078c1a2c00/crisp_64lksp.png"}
	message := crispAPI.ConversationFileMessageNew{
		Type:    "file",
		From:    "operator",
		Origin:  "chat",
		User:    user,
		Content: content,
	}

	_, _, err := crispManager.client.Website.SendFileMessageInConversation("e93e073a-1f69-4cbc-8934-f9e1611e65bb", sessionID, message)

	if err != nil {
		fmt.Printf("File not sent: %v\n", err)
	}

	err = os.Remove(string(sessionID + ".txt"))

	if err != nil {
		fmt.Printf("Delete file error: %v\n\n", err)
	}
}
