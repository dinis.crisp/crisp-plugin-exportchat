package main

import (
	"log"
	"os"
)

func main() {

	crispManager := initCrisp()

	err := crispManager.initHTTPEndpoint()

	log.Panic("HTTP endpoint crashed: ", err)

	os.Exit(1)

}
