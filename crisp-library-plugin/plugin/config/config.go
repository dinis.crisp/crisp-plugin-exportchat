package crispPluginConfig

import (
	"bufio"
	"bytes"
	"flag"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/BurntSushi/toml"
)

type ConfigBase struct {
	CrispRESTEndpointURL     string `toml:"crisp_rest_endpoint_url"`
	CrispRESTAuthIdentifer   string `toml:"crisp_rest_auth_identifier"`
	CrispRESTAuthKey         string `toml:"crisp_rest_auth_key"`
	CrispRealtimeEndpointURL string `toml:"crisp_realtime_endpoint_url"`
	HTTPAPIListen            string `toml:"http_api_listen"`
}

var (
	configLineVariableRegex = regexp.MustCompile(`^(?P<key>[^\s=]+)(?P<separator>\s?=\s?)"\$\{(?P<value>[^\s\$\{\}]+)\}"$`)
)

// Load loads all configuration values from file to target structure
func Load(config interface{}) {
	if _, err := toml.Decode(read(), config); err != nil {
		panic(err)
	}
}

func read() string {
	// Acquire file path (depends on environment)
	var env = flag.String("env", "production", "Runtime environment")
	flag.Parse()

	basepath, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	configFilePath := path.Join(basepath, strings.Join([]string{"./config/", *env, ".toml"}, ""))

	// Open configuration file
	configFile, err := os.Open(configFilePath)

	if err != nil {
		panic("Config file is missing or unreadable at: " + configFilePath)
	}

	// Read configuration file, line by line
	var configBytes bytes.Buffer

	scanner := bufio.NewScanner(configFile)

	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		configBytes.WriteString(transformLine(scanner.Text()))
		configBytes.WriteString("\n")
	}

	// Close configuration file
	configFile.Close()

	return configBytes.String()
}

func transformLine(line string) string {
	// Ensure parsed line is clear from prefix and suffix spaces
	line = strings.TrimSpace(line)

	// Transform line value from environment variable path to underlying value?
	// Eg. config_key = "${ENVIRONMENT_VARIABLE_PATH}"
	match := configLineVariableRegex.FindStringSubmatch(line)

	if len(match) > 0 && match[1] != "" && match[2] != "" && match[3] != "" {
		// Expand variable to its value
		lineVariableName := match[3]
		lineVariableValue := os.Getenv("CONFIG_" + lineVariableName)

		if lineVariableValue == "" {
			panic("Config file variable could not be expanded: " + lineVariableName)
		}

		// Re-generate expanded configuration line
		line = (match[1] + match[2] + "\"" + lineVariableValue + "\"")
	}

	return line
}
