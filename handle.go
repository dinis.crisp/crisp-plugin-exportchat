package main

import (
	"bytes"
	"fmt"
	"os"
	"time"

	crispAPI "github.com/crisp-im/go-crisp-api/crisp"
	"github.com/mitchellh/mapstructure"
)

func (crispManager *crisp) getTranscript(response SubmitButtonItem) {
	var LastMessageFrom string

	var empty []string

	t := time.Now().UnixNano()

	conversation, res, err := crispManager.client.Website.GetMessagesInConversationBefore(response.Origin.WebsiteID, response.Origin.SessionID, uint(t))

	if err != nil {
		fmt.Printf("Error: %v", err)
	} else {
		crispManager.WriteTranscript(*conversation, LastMessageFrom, *res, empty)

	}
}

func (crispManager *crisp) WriteTranscript(conversation []crispAPI.ConversationMessage, LastMessageFrom string, res crispAPI.Response, preConversationMessages []string) {

	var firstC crispAPI.ConversationMessage
	var conversationMessages []string

	for i, c := range conversation {
		if i == 0 {
			firstC = c
		}

		if LastMessageFrom == "" {
			LastMessageFrom = *c.From

			l := string("\n" + *c.User.Nickname + "(" + *c.From + "): \n")

			conversationMessages = append(conversationMessages, l)
		}

		if LastMessageFrom != *c.From {
			l := string("\n** " + *c.User.Nickname + " (" + *c.From + "): \n")

			conversationMessages = append(conversationMessages, l)
		}

		if *c.Type == "picker" {
			var content crispAPI.ConversationMessagePickerContent
			var answer string

			err := mapstructure.Decode(*c.Content, &content)

			if err != nil {
				println("Error: %v", err)
			}

			for _, c := range *content.Choices {
				if *c.Selected {
					answer = *c.Label
				}
			}

			msgT := formatMessageTime(*c.Timestamp)

			l := string("   [" + msgT + "] Q:" + *content.Text + " A:" + answer + "\n")

			conversationMessages = append(conversationMessages, l)

		} else if *c.Type == "event" {

			var content struct {
				Namespace string
			}

			err := mapstructure.Decode(*c.Content, &content)

			if err != nil {
				println("ErrorOORR: %v", err)
			}

			msgT := formatMessageTime(*c.Timestamp)

			l := string("   [" + msgT + "] Conversation " + content.Namespace + "! \n")

			conversationMessages = append(conversationMessages, l)
		} else if *c.Type == "text" {

			msgT := formatMessageTime(*c.Timestamp)

			msgC := *c.Content
			msgCont := msgC.(string)
			r := []rune("\n")
			var buffer bytes.Buffer

			for _, char := range msgCont {
				buffer.WriteRune(char)
				if char == r[0] {
					for i := 0; i < 22; i++ {
						buffer.WriteRune(' ')
					}
				}
			}

			l := string("   [" + msgT + "] " + buffer.String() + "\n")

			conversationMessages = append(conversationMessages, l)
		}

		LastMessageFrom = *c.From

	}

	conversationMessages = append(conversationMessages, preConversationMessages...)

	crispManager.checkStatus(res, firstC, conversationMessages)

}

func (crispManager *crisp) checkStatus(res crispAPI.Response, firstC crispAPI.ConversationMessage, conversationMessages []string) {

	if res.StatusCode == 206 {
		conversation, res, err := crispManager.client.Website.GetMessagesInConversationBefore(*firstC.WebsiteID, *firstC.SessionID, uint(*firstC.Timestamp))

		if err != nil {
			fmt.Printf("Error: %v", err)
		}

		crispManager.WriteTranscript(*conversation, *firstC.From, *res, conversationMessages)
	} else {
		f, _ := os.Create(string(*firstC.SessionID) + ".txt")

		f.WriteString("Covnersation Transcript!\n")

		for _, ms := range conversationMessages {
			f.WriteString(ms)
		}

		fmt.Printf("Transcript Saved! \n")
	}
}

func formatMessageTime(i uint64) string {

	t := time.Unix(int64(i)/1000, 000)
	msgTimeUTC := t.Format("2006/01/02 15:04")

	return msgTimeUTC
}
