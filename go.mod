module example.com/crisp

go 1.17

replace example.com/crisp => ./

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/crisp-im/go-crisp-api v3.12.0+incompatible
	github.com/mitchellh/mapstructure v1.4.2
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/graarh/golang-socketio v0.0.0-20170510162725-2c44953b9b5f // indirect
	golang.org/x/text v0.3.7
)
